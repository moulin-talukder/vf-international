<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Models\Account;
use \App\Models\DebitCreditNote;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $account = Account::all();
        return view('note.notes', compact('account'));
    }


    // public function addField(Request $request)
    // {
        
     
    //     foreach ($request->multiInput as $key => $value) {
    //         Note::create($value);
    //     }
     
    //     return back()->with('success', 'Form has been submitted.');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
         return $request->all();
        // $validateData = $request->validate([
        //     'bank_account' => 'required|max:255',
        //     'bank_name' => 'required|unique:accounts|max:255',
        //     'bank_swift' => 'required|unique:accounts|max:255',
        //     'bank_address' => 'required',
        //   ]);
  
        //   $data=array();
        //   $data['customer_id']=$request->customer_id;
        //   $data['customer_name']=$request->customer_name;
        //   $data['customer_address']=$request->customer_address;
        //   $data['consignor']=$request->consignor;
        //   $data['consignee']=$request->consignee;
        //   $data['container_no']=$request->container_no;
        //   $data['weight']=$request->weight;
        //   $data['volume']=$request->volume;
        //   $data['chargeable']=$request->chargeable;
        //   $data['packages']=$request->packages;
        //   $data['shipment']=$request->shipment;
        //   $data['value']=$request->value;
        //   $data['vf_date']=$request->vf_date;
        //   $data['consignee']=$request->consignee;
        //   $data['mbl_mawb']=$request->mbl_mawb;
        //   $data['hbl_hawb']=$request->hbl_hawb;
        //   $data['origin']=$request->origin;
        //   $data['etd']=$request->etd;
        //   $data['destination']=$request->destination;
        //   $data['total']=$request->total;
        //   $data['vat']=$request->vat;
        //   $data['account_id']=$request->account_id;
        //   $purpose = $request->get('purpose[]');
        //   $amount = $request->get('amount[]');
          
          


          
          Account::insert($data);
  
              return redirect()->back()->with('message','Note Created Successfully.');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
