<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Account;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = Account::all();
        return view('account.accounts', compact('account'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        return view('account.accounts_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'bank_account' => 'required|max:255',
            'bank_name' => 'required|unique:accounts|max:255',
            'bank_swift' => 'required|unique:accounts|max:255',
            'bank_address' => 'required',
          ]);
  
          $data=array();
          $data['bank_account']=$request->bank_account;
          $data['bank_name']=$request->bank_name;
          $data['bank_swift']=$request->bank_swift;
          $data['bank_address']=$request->bank_address;
          $data['status']=$request->status;
          
          


          
          Account::insert($data);
  
              return redirect()->route('account.show')->with('message','Account Created Successfully.');


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Account::findOrFail($id);
        //dd($edit);
       return view('account.accounts_edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        $request->validate([
            'bank_account' => 'required|max:255',
            'bank_name' => 'required|max:255',
            'bank_swift' => 'required|max:255',
            'bank_address' => 'required',
         ]);

        $account = Account::find($id);
        $account->bank_account = $request->bank_account;
        $account->bank_name = $request->bank_name;
        $account->bank_swift = $request->bank_swift;
        $account->bank_address = $request->bank_address;
        $account->status = $request->status;

        $account->save();
        return redirect()->route('account.show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Account::find($id)->delete();

        return redirect()->back();
    }
}
