<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\Inventory\StoreController;
use App\Http\Controllers\Inventory\PartsController;
use App\Http\Controllers\Inventory\RackController;
use App\Http\Controllers\Inventory\BinController;
use App\Http\Controllers\Inventory\ProductSourcingVendorController;
use App\Http\Controllers\Inventory\ServiceSourcingVendorController;
use App\Http\Controllers\Inventory\InventoryController;
use App\Http\Controllers\Inventory\BrandController;
use App\Http\Controllers\Inventory\FaultController;
use App\Http\Controllers\Inventory\CategoryController;
use App\Http\Controllers\Inventory\ProductCategoryController;
use App\Http\Controllers\Inventory\RegionController;
use App\Http\Controllers\Inventory\DirectPartsSellController;
use App\Http\Controllers\Inventory\ModelController;
use App\Http\Controllers\Inventory\PriceManagementController;
use App\Http\Controllers\Inventory\CentralPartsReturnController;
use App\Http\Controllers\Inventory\PartsReturnController;
use App\Http\Controllers\Ticket\WarrantyTypeController;
use App\Http\Controllers\Ticket\ServiceTypeController;
use App\Http\Controllers\Ticket\PurchaseHistoryController;
use App\Http\Controllers\Employee\EmployeeController;
use App\Http\Controllers\Ticket\JobPriorityController;
use App\Http\Controllers\Ticket\ProductConditionController;
use App\Http\Controllers\Ticket\AccessoriesController;
use App\Http\Controllers\Ticket\ReceiveModeController;
use App\Http\Controllers\Ticket\DeliveryModeController;
use App\Http\Controllers\Outlet\OutletController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Account\ExpenseController;
use App\Http\Controllers\Account\RevenueController;
use App\Http\Controllers\Account\BankAccountController;
use App\Http\Controllers\Account\DepositController;
use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Employee\DesignationController;
use App\Http\Controllers\Requisition\RequisitionController;
use App\Http\Controllers\Requisition\PendingRequisitonController;
use App\Http\Controllers\Requisition\TechnicianRequisitionController;
use App\Http\Controllers\ProductPurchase\ProductPurchaseController;
use App\Http\Controllers\Job\JobController;
use App\Http\Controllers\Job\JobSubmissionController;
use App\Http\Controllers\Group\GroupController;
use App\Http\Controllers\NoteController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () { return view('auth.login'); });


Route::get('login', [LoginController::class,'showLoginForm'])->name('login');
Route::post('login', [LoginController::class,'login']);
Route::post('register', [RegisterController::class,'register']);

Route::get('password/forget',  function () {
	return view('pages.forgot-password');
})->name('password.forget');
Route::post('password/email', [ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class,'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class,'reset'])->name('password.update');


Route::group(['middleware' => 'auth'], function(){
	// logout route
	Route::get('/logout', [LoginController::class,'logout']);
	Route::get('/clear-cache', [HomeController::class,'clearCache']);
	// dashboard route
	Route::get('/dashboard', function () {
		return view('pages.dashboard');
	})->name('dashboard');

    // get permissions
	Route::get('get-role-permissions-badge', [PermissionController::class,'getPermissionBadgeByRole']);
	// permission examples
    Route::get('/permission-example', function () {
    	return view('permission-example');
    });
    // API Documentation
    Route::get('/rest-api', function () { return view('api'); });
    // Editable Datatable
	Route::get('/table-datatable-edit', function () {
		return view('pages.datatable-editable');
	});

    Route::group(['namespace' => 'Inventory'], function () {
        //only those have manage_user permission will get access
        Route::group(['middleware' => 'can:manage_user'], function(){
        Route::get('/users', [UserController::class,'index']);
        Route::get('/user/get-list', [UserController::class,'getUserList']);
        Route::get('/user/create', [UserController::class,'create']);
        Route::post('/user/create', [UserController::class,'store'])->name('create-user');
        Route::get('/user/{id}', [UserController::class,'edit']);
        Route::post('/user/update', [UserController::class,'update']);
        Route::get('/user/delete/{id}', [UserController::class,'delete']);
        });


        //account
        Route::get('/accounts', [AccountController::class,'index'])->name('account.show');
        Route::get('/accounts/form', [AccountController::class,'form'])->name('account.form');
        Route::post('/accounts/create', [AccountController::class,'store'])->name('account.create');
        Route::get('/accounts/{id}', [AccountController::class,'edit']);
        Route::post('/accounts/update/{id}', [AccountController::class,'update'])->name('account.update');
        Route::get('/accounts/delete/{id}', [AccountController::class,'destroy'])->name('account.delete');


        


        //note
        Route::get('/notes', [NoteController::class,'index'])->name('note.show');
        Route::post('/notes/create', [NoteController::class,'store'])->name('note.create');
        // Route::post('/notes/addfield', [NoteController::class, 'addField'])->name('field.add');





        //only those have manage_role permission will get access
        Route::group(['middleware' => 'can:manage_role|manage_user'], function(){
            Route::get('/roles', [RolesController::class,'index']);
            Route::get('/role/get-list', [RolesController::class,'getRoleList']);
            Route::post('/role/create', [RolesController::class,'create']);
            Route::get('/role/edit/{id}', [RolesController::class,'edit']);
            Route::post('/role/update', [RolesController::class,'update']);
            Route::get('/role/delete/{id}', [RolesController::class,'delete']);
        });

        //only those have manage_permission permission will get access
        Route::group(['middleware' => 'can:manage_permission|manage_user'], function(){
            Route::get('/permission', [PermissionController::class,'index']);
            Route::get('/permission/get-list', [PermissionController::class,'getPermissionList']);
            Route::post('/permission/create', [PermissionController::class,'create']);
            Route::get('/permission/update', [PermissionController::class,'update']);
            Route::get('/permission/delete/{id}', [PermissionController::class,'delete']);
        });

    });

});


// Route::get('/register', function () { return view('pages.register'); });
// Route::get('/login-1', function () { return view('pages.login'); });
