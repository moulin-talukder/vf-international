<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
	<title>@yield('title','') | Rangs</title>
	<!-- initiate head with meta tags, css and script -->
	@include('include.head')
<!-- jQuery library -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">

<!-- Popper JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>


<style>
	h3{
		padding: 7px;
	}

	button.remove-item.btn.btn-danger {
    margin: 0% 91%;
    }

	/* #addRemoveIp {
    margin: 0em 23em;
	} */

	.c1{
		float:left;
		margin-left: 25px;
		font-size: 19px;
		padding: 0.5% 0%;
		
	}

	.c2{
		float:right;
		margin: 4px 50px;
	}

	.c{
		background: white;
		border: 1px solid lightgray;
        border-radius: 0.3rem;
	}

	.c::after{
		content: "";
        display: block;
        clear: both;
	}

	i.fa.fa-plus {
    font-size: 21px;
    }

	i.fa.fa-minus {
    font-size: 21px;
    }



	input.btn.btn-dark.btn-block {
    padding-bottom: 3%;
    }

</style>
</head>
<body id="app" >
    <div class="wrapper">
    	<!-- initiate header-->
    	@include('include.header')
    	<div class="page-wrap">
	    	<!-- initiate sidebar-->
	    	@include('include.sidebar')

	    	<div class="main-content">
	    		<!-- yeild contents here -->
	    		@yield('content')
	    	</div>

	    	<!-- initiate chat section-->
	    	@include('include.chat')


	    	<!-- initiate footer section-->
	    	@include('include.footer')

    	</div>
    </div>
    
	<!-- initiate modal menu section-->
	@include('include.modalmenu')

	<!-- initiate scripts-->
	@include('include.script')	
</body>
</html>