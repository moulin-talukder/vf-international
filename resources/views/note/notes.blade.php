@extends('layouts.main') 
@section('title', 'Users')
@section('content')

    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    @endpush

    
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Add Note')}}</h5>
                            <span>{{ __('Create new note, assign roles & permissions')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Add Note')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">
                        <h3>{{ __('Add customer')}}</h3>
                    </div>
                    <div class="card-body">
                    <form class="forms-sample" method="POST" action="{{ route('note.create')}}" >
                        @csrf
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="customer_name">{{ __('Customer Name')}}<span class="text-red">*</span></label>
                                        <input id="customer_name" type="text" class="form-control @error('customer_name') is-invalid @enderror" name="bank_account" value="" placeholder="Enter name" required>
                                        <div class="help-block with-errors"></div>

                                        @error('customer_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="customer_address">{{ __('Customer Address')}}<span class="text-red">*</span></label>
                                        <textarea id="customer_address" type="text" class="form-control @error('customer_address') is-invalid @enderror" name="customer_address" value="" placeholder="Enter address" required rows="3"></textarea>
                                        <div class="help-block with-errors" ></div>

                                        @error('customer_address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                   
                                    
                                    
                                    
                                
                                </div>

                                <div class="col-sm-6">
                                <div class="form-group">
                                        <label for="customer_id">{{ __('Customer ID')}}<span class="text-red">*</span></label>
                                        <input id="customer_id" type="number" class="form-control" name="customer_id" placeholder="Enter ID" required>
                                        <div class="help-block with-errors"></div>

                                        @error('customer_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    
                                </div>

                            </div>
                    </div>
                </div>



                    <!-- here -->
                    <div class="card">

                                <!-- shipment details -->
                                <div class="card-header">
                                <h3>{{ __('Shipment Details')}}</h3>
                            </div>

                        <div class="card-body">

                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="consignor">{{ __('Consignor')}}<span class="text-red">*</span></label>
                                        <input id="consignor" type="text" class="form-control @error('consignor') is-invalid @enderror" name="consignor" value="" placeholder="Consignor" required>
                                        <div class="help-block with-errors"></div>

                                        @error('consignor')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="container_no">{{ __('Container NO')}}<span class="text-red">*</span></label>
                                        <input id="container_no" type="text" class="form-control @error('container_no') is-invalid @enderror" name="container_no" value="" placeholder="Enter Container NO" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('container_no')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-sm-6">
                                <div class="form-group">
                                        <label for="consignee">{{ __('Consignee')}}<span class="text-red">*</span></label>
                                        <input id="consignee" type="text" class="form-control" name="consignee" placeholder="Enter Consignee" required>
                                        <div class="help-block with-errors"></div>

                                        @error('consignee')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    
                                </div>

                            </div>
                                

                        </div>
                     
                </div>



                    <!-- goods description -->
                <div class="card ">
                            <div class="card-header">
                                <h3>{{ __('Goods Description')}}</h3>
                            </div>

                     <div class="card-body">

                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="weight">{{ __('Weight')}}<span class="text-red">*</span></label>
                                        <input id="weight" type="number" class="form-control @error('weight') is-invalid @enderror" name="weight" value="" placeholder="Weight" required>
                                        <div class="help-block with-errors"></div>

                                        @error('weight')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="volume">{{ __('Volume')}}<span class="text-red">*</span></label>
                                        <input id="volume" type="number" class="form-control @error('volume') is-invalid @enderror" name="volume" value="" placeholder="Enter Volume" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('volume')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="chargeable">{{ __('Chargeable')}}<span class="text-red">*</span></label>
                                        <input id="chargeable" type="number" class="form-control @error('chargeable') is-invalid @enderror" name="chargeable" value="" placeholder="Enter Chargeable" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('chargeable')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="packages">{{ __('Packages')}}<span class="text-red">*</span></label>
                                        <input id="packages" type="number" class="form-control @error('packages') is-invalid @enderror" name="packages" value="" placeholder="Enter packages" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('packages')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="packages">{{ __('Shipment')}}<span class="text-red">*</span></label>
                                    <select class="form-select form-select-sm form-control" name="shipment" aria-label=".form-select-sm example">
                                        <option selected>Choose One</option>
                                        <option value="Vessel">Vessel</option>
                                        <option value="Flight">Flight</option>
                                    </select>
                                        <div class="help-block with-errors" ></div>
  
                                    </div>



                                    <div class="form-group">
                                        <input id="value" type="text" class="form-control @error('date') is-invalid @enderror" name="value" value="" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('value')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="vf_date">{{ __('Date')}}<span class="text-red">*</span></label>
                                        <input id="vf_date" type="text" class="form-control @error('vf_date') is-invalid @enderror" name="vf_date" value="" placeholder="Enter Date" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('vf_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    
                                    
                                    
                                
                                </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                        <label for="mbl_mawb">{{ __('MBL/MAWB')}}<span class="text-red">*</span></label>
                                        <input id="mbl_mawb" type="number" class="form-control" name="mbl_mawb" placeholder="Enter MBL/MAWB" required>
                                        <div class="help-block with-errors"></div>

                                        @error('mbl_mawb')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                 </div>

                                <div class="form-group">
                                        <label for="hbl_hawb">{{ __('HBL/HAWB')}}<span class="text-red">*</span></label>
                                        <input id="hbl_hawb" type="text" class="form-control" name="hbl_hawb" placeholder="Enter HBL/HAWB" required>
                                        <div class="help-block with-errors"></div>

                                        @error('hbl_hawb')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                 </div>
                                <div class="form-group">
                                        <label for="origin">{{ __('Origin')}}<span class="text-red">*</span></label>
                                        <input id="origin" type="text" class="form-control" name="origin" placeholder="Enter Origin" required>
                                        <div class="help-block with-errors"></div>

                                        @error('origin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                 </div>
                                <div class="form-group">
                                        <label for="etd">{{ __('ETD')}}<span class="text-red">*</span></label>
                                        <input id="etd" type="text" class="form-control" name="etd" placeholder="Enter ETD" required>
                                        <div class="help-block with-errors"></div>

                                        @error('etd')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                 </div>
                                <div class="form-group">
                                        <label for="destination">{{ __('Destination')}}<span class="text-red">*</span></label>
                                        <input id="destination" type="text" class="form-control" name="destination" placeholder="Enter Destination" required>
                                        <div class="help-block with-errors"></div>

                                        @error('destination')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                 </div>
                                    
                 </div>

            </div>

                        </div>

                    </div>

                <!-- charge and description -->
                    
                            <div class="c">
                                <div class="c1">
                                <h5 class="head">{{ __('Charges and Description')}}</h5>
                                </div>
                                
                                <div class="c2">
                                <button type="button" name="add" value="" id="addRemoveIp" class="btn btn-info"><i class="fa fa-plus"></i></button>
                                </div>

                            </div>
                            
                            <div class="card">
            <div class="card-body" id="multiForm">

                <div class="row">
                    <div class="col-sm-6">

                        <div class="form-group">
                                    <label for="packages">{{ __('Charges in')}}<span class="text-red">*</span></label>
                                <select class="form-select form-select-sm form-control" aria-label=".form-select-sm example">
                                    <option selected>Choose One</option>
                                    <option value="1">BDT</option>
                                    <option value="2">USD</option>
                                </select>
                                    <div class="help-block with-errors" ></div>

                        </div>
                    </div>
                </div>



            <div class="row ap">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="purpose-0">{{ __('Purpose')}}<span class="text-red">*</span></label>
                            <input id="purpose-0" type="text" class="form-control"  name="purpose[]" placeholder="Purpose" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                                <label for="amount-0">{{ __('Amount')}}<span class="text-red">*</span></label>
                                <input id="amount-0" type="number" class="form-control" name="amount[]" placeholder="Amount" required>

                                
                        <div class="help-block with-errors"></div>

                    </div>

               </div>
            
            </div>

        </div>
 
        </div>

<div class="card">

<!-- total charges -->
<div class="card-header">
<h3>{{ __('Total Charges')}}</h3>
</div>

<div class="card-body">

<div class="row">
<div class="col-sm-6">

    <div class="form-group">
        <label for="sub_total">{{ __('Sub Total')}}<span class="text-red">*</span></label>
        <input id="sub_total" type="number" class="form-control @error('sub_total') is-invalid @enderror" name="sub_total" value="" placeholder="Sub Total" required>
        <div class="help-block with-errors"></div>

        @error('consignor')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="total">{{ __('Total in BDT')}}<span class="text-red">*</span></label>
        <input id="total" type="number" class="form-control @error('total') is-invalid @enderror" name="total" value="" placeholder="Enter Total in BDT" required>
        <div class="help-block with-errors" ></div>

        @error('total')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
   
    
    
    

</div>

<div class="col-sm-6">
   
<div class="form-group">
        <label for="vat">{{ __('Add VAT')}}<span class="text-red">*</span></label>
        <input id="vat" type="number" class="form-control @error('vat') is-invalid @enderror" name="vat" value="" placeholder="Enter VAT" required>
        <div class="help-block with-errors" ></div>

        @error('vat')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
</div>


<div class="form-group">
        <label for="bank_account">{{ __('Bank Account')}}<span class="text-red">*</span></label><br>
        <select class="form-select form-select-sm form-control" aria-label=".form-select-sm example">
            <option selected>Choose One</option>
            @foreach($account as $ac)
            <option value="{{$ac->id}}">{{$ac->bank_name}}</option>
            @endforeach
        </select>
        <div class="help-block with-errors"></div>

        @error('bank_account')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    
</div>

</div>


</div>

<div class="d-grid mt-3">
    <input type="submit" class="btn btn-dark btn-block" value="Submit">
</div> 

</div>

     
</form>
</div>

@push('script')
    <script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
    <!--server side roles table script-->
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
    $(document).ready(function(){
        var i = 1;
    $("#addRemoveIp").click(function () {
        alert(i);
        ++i;
        $("#multiForm").append("<div class='row ap'>"+"<div class='col-sm-6'>"+"<div class='form-group'>"+"<label for='purpose-0'>{{ __('Purpose')}}"+"<span class='text-red'>*</span>"+"</label>"+"<input id='purpose-'+i+'' type='text' class='form-control'  name='purpose[]' placeholder='Purpose' required>"+"<div class='help-block with-errors'></div>"+"</div>"+"</div>"+"<div class='col-sm-6'>"+"<div class='form-group'>"+"<label for='amount-0'>{{ __('Amount')}}"+"<span class='text-red'>*</span>"+"</label>"+"<input id='amount-'+i+'' type='number' class='form-control' name='amount[]' placeholder='Amount' required>"+"<div class='help-block with-errors'></div>"+"</div>"+"</div>"+"<button type='button' class='remove-item btn btn-danger'>"+"<i class='fa fa-minus'></i>"+"</button>");
    });
    $(document).on('click', '.remove-item', function () {
        $(this).parents('.ap').remove();
    });
    });
    
</script>
@endpush
    
@endsection