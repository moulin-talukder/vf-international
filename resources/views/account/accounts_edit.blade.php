@extends('layouts.main') 
@section('title', 'Users')
@section('content')

    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    @endpush

    
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Edit Account')}}</h5>
                            <span>{{ __('Edit account')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Edit Account')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">
                        <h3>{{ __('Edit account')}}</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample" method="POST" action="{{route('account.update',$edit->id)}}" enctype="multipart/form-data" >
                        @csrf
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="bank_account">{{ __('Bank Account')}}<span class="text-red">*</span></label>
                                        <input id="bank_account" type="text" class="form-control @error('bank_account') is-invalid @enderror" name="bank_account" value="{{$edit->bank_account}}" placeholder="Enter user name" required>
                                        <div class="help-block with-errors"></div>

                                        @error('bank_account')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_name">{{ __('Bank Name')}}<span class="text-red">*</span></label>
                                        <input id="bank_name" type="text" class="form-control @error('bank_name') is-invalid @enderror" name="bank_name" value="{{$edit->bank_name}}" placeholder="Enter bank name" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('bank_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                   
                                    
                                    
                                    
                                
                                </div>


                                <div class="col-sm-6">
                                <div class="form-group">
                                        <label for="bank_swift">{{ __('Bank Swift')}}<span class="text-red">*</span></label>
                                        <input id="bank_swift" type="text" class="form-control @error('bank_swift') is-invalid @enderror" name="bank_swift" placeholder="Bank Swift" value="{{$edit->bank_swift}}" required>
                                        <div class="help-block with-errors"></div>

                                        @error('bank_swift')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_address">{{ __('Bank Address')}}<span class="text-red">*</span></label>
                                        <input id="bank_address" type="text" class="form-control bank_address" name="bank_address" placeholder="Bank Address" value="{{$edit->bank_address}}" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <input type="hidden" name="status" value="1">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">{{ __('Update')}}</button>
                                    </div>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection