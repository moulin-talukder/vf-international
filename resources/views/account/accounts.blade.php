@extends('layouts.main') 
@section('title', 'Users')
@section('content')
<div class="content-page">
                <div class="content">
                    <div class="container">

                        

                        <!-- Start Widget -->
                        <div class="row">
                              <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Accounts
                                        <a href="{{ route('account.form') }}" class="btn btn-sm btn-info pull-right">Add New</a>
                                        </h3>
                                        
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Bank Account</th>
                                                            <th>Bank Name</th>
                                                            <th>Bank Swift</th>
                                                            <th>Bank Address</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>

                                             
                                                    <tbody>
                                                    @foreach($account as $row)
                                                        <tr>
                                                            <td>{{ $row->bank_account }}</td>
                                                            <td>{{ $row->bank_name }}</td>
                                                            <td>{{ $row->bank_swift }}</td>
                                                            <td>{{ $row->bank_address }}</td>
                                                            <td>
                                                            <a href="{{ URL::to('/accounts/'.$row->id)}}" class=""><i class="ik ik-edit-2 f-16 mr-15 text-green"></i></a>
                                                            <a href="{{ URL::to('/accounts/delete/'.$row->id)}}" class="" id="delete"><i class="ik ik-trash-2 f-16 text-red"></i></a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div> <!-- container -->
                               
                </div> <!-- content -->
            </div>
@endsection
