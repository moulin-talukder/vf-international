@extends('layouts.main') 
@section('title', 'Users')
@section('content')

    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    @endpush

    
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Add Account')}}</h5>
                            <span>{{ __('Create new account, assign roles & permissions')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Add Account')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">
                        <h3>{{ __('Add account')}}</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample" method="POST" action="{{ route('account.create') }}" >
                        @csrf
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="name">{{ __('Bank Account')}}<span class="text-red">*</span></label>
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="bank_account" value="" placeholder="Enter user name" required>
                                        <div class="help-block with-errors"></div>

                                        @error('bank_account')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_name">{{ __('Bank Name')}}<span class="text-red">*</span></label>
                                        <input id="bank_name" type="text" class="form-control @error('email') is-invalid @enderror" name="bank_name" value="" placeholder="Enter email address" required>
                                        <div class="help-block with-errors" ></div>

                                        @error('bank_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                   
                                    
                                    
                                    
                                
                                </div>

                                <div class="col-sm-6">
                                <div class="form-group">
                                        <label for="bank_swift">{{ __('Bank Swift')}}<span class="text-red">*</span></label>
                                        <input id="bank_swift" type="text" class="form-control" name="bank_swift" placeholder="Bank Swift" required>
                                        <div class="help-block with-errors"></div>

                                        @error('bank_swift')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="bank_address">{{ __('Bank Address')}}<span class="text-red">*</span></label>
                                        <input id="bank_address" type="text" class="form-control" name="bank_address" placeholder="Bank Address" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <input type="hidden" name="status" value="1">
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                                    </div>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection