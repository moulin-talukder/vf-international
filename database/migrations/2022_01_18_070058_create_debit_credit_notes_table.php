<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebitCreditNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_credit_notes', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->string('customer_name');
            $table->string('customer_address');
            $table->string('consignor');
            $table->string('consignee');
            $table->double('container_no', 15,2);
            $table->double('weight', 15,2);
            $table->double('volume', 15,2);
            $table->double('chargeable', 15,2);
            $table->double('packages', 15,2);
            $table->string('shipment');
            $table->double('value', 15,2);
            $table->date('vf_date');
            $table->double('mbl_mawb', 15,2);
            $table->string('hbl_hawb');
            $table->string('origin');
            $table->date('etd');
            $table->string('destination');
            $table->double('sub_total');
            $table->double('total');
            $table->double('vat');
            $table->integer('account_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debit_credit_notes');
    }
}
